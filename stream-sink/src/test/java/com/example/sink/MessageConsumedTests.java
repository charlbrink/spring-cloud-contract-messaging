package com.example.sink;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.cloud.contract.stubrunner.StubTrigger;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.NONE,
        properties = "spring.cloud.stream.bindings.input.destination=sensor-data")
@AutoConfigureStubRunner(stubsMode = StubRunnerProperties.StubsMode.LOCAL)
public class MessageConsumedTests {
    @Autowired
    StubTrigger stubTrigger;

    @Autowired
    Listener listener;

    @Test
    public void testMessageConsumed() throws Exception {
        int count = this.listener.getCount();
        stubTrigger.trigger("sensor1");
        assertThat(this.listener.getCount()).isEqualTo(count + 1);
    }
}
